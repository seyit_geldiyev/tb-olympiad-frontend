import type { NuxtConfig } from "@nuxt/types";

const config: NuxtConfig = {
  ssr: true,
  target: "server",

  server: {
    host: process.env.HOST || "127.0.0.1",
    port: process.env.PORT || 3000,
  },

  axios: {
    proxy: true,
  },

  proxy: {
    "/api/v1/": {
      target: process.env.BACKEND_SERVER_ORIGIN || "http://127.0.0.1:8080",
      changeOrigin: true,
    },
  },

  srcDir: "src/",

  head: {
    title: "Спорт комплексы",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  css: ["~/assets/scss/style.scss"],

  plugins: [
    "plugins/charts.ts",
    "plugins/http.ts",
    "plugins/loading.ts",
    {
      src: "plugins/vue-yandex-maps.ts",
      mode: "client",
    },
  ],

  components: true,

  buildModules: ["@nuxt/typescript-build", "@nuxtjs/tailwindcss"],

  modules: ["@nuxtjs/axios"],

  tailwindcss: {
    configPath: "../tailwind.config.js",
  },

  styleResources: {
    scss: "./src/assets/scss/base/_variables.scss",
  },

  build: {},
};

export default config;
