import type { Plugin, Context } from "@nuxt/types";
import type { AxiosRequestConfig, AxiosResponse } from "axios";
import { getUrlPrefixedApi } from "@/utils";

declare module "vue/types/vue" {
  interface Vue {
    $http: HttpApiService;
  }
}

declare module "@nuxt/types" {
  interface NuxtAppOptions {
    $http: HttpApiService;
  }
  interface Context {
    $http: HttpApiService;
  }
}

export class HttpApiService {
  private interceptor: number;
  private ctx: Context;

  // eslint-disable-next-line no-use-before-define
  api: HttpApiService;

  constructor(ctx: Context) {
    this.interceptor = 0;
    this.ctx = ctx;

    this.api = getUrlPrefixedApi(this, "/api/v1/");
  }

  private async load<T>(callback: () => Promise<AxiosResponse<T>>) {
    this.ctx.$load.increase();
    try {
      return await callback();
    } finally {
      this.ctx.$load.decrease();
    }
  }

  get<RT>(
    url: string,
    query = "",
    config: AxiosRequestConfig = {},
    loading: boolean = true
  ) {
    if (query !== "") {
      url = `${url}?${query}`;
    }

    if (loading) {
      return this.load(() => this.ctx.$axios.get<RT>(url, config));
    }

    return this.ctx.$axios.get<RT>(url, config);
  }

  post<T, RT>(
    url: string,
    data?: T,
    config?: AxiosRequestConfig,
    loading: boolean = true
  ) {
    if (loading) {
      return this.load(() => this.ctx.$axios.post<RT>(`${url}`, data, config));
    }

    return this.ctx.$axios.post<RT>(`${url}`, data, config);
  }

  put<T, RT>(
    url: string,
    data?: T,
    config: AxiosRequestConfig = {},
    loading: boolean = true
  ) {
    if (loading) {
      return this.load(() => this.ctx.$axios.put<RT>(`${url}`, data, config));
    }

    return this.ctx.$axios.put<RT>(`${url}`, data, config);
  }

  delete<RT>(
    url: string,
    config: AxiosRequestConfig = {},
    loading: boolean = true
  ) {
    if (loading) {
      return this.load(() => this.ctx.$axios.delete<RT>(`${url}`, config));
    }

    return this.ctx.$axios.delete<RT>(`${url}`, config);
  }
}

const p: Plugin = function (ctx, inject) {
  const $http = new HttpApiService(ctx);
  inject("http", $http);
};

export default p;
