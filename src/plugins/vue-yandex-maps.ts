import Vue from "vue";
import YmapsPlugin from "vue-yandex-maps";

const settings = {
  apiKey: "",
  lang: "ru_RU",
  coordorder: "latlong",
  enterprise: false,
  version: "2.1",
};

Vue.use(YmapsPlugin, settings);
