import { Plugin } from "@nuxt/types";
import Vue from "vue";

class NetworkLoadingService {
  reactive: {
    count: number;
  };

  constructor() {
    this.reactive = Vue.observable<{ count: 0 }>({ count: 0 });
  }

  isLoading() {
    return this.reactive.count > 0;
  }

  increase() {
    this.reactive.count++;
  }

  decrease() {
    if (this.reactive.count > 0) {
      this.reactive.count--;
    }
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $load: NetworkLoadingService;
  }
}

declare module "@nuxt/types" {
  interface NuxtAppOptions {
    $load: NetworkLoadingService;
  }
  interface Context {
    $load: NetworkLoadingService;
  }
}

const p: Plugin = function (_, inject) {
  const loading = new NetworkLoadingService();
  inject("load", loading);
};

export default p;
