export interface ApiSportComplexType {
  id: number;
  name: string;
  count: number;
}

export interface ApiSportComplex {
  id: number;
  name_ru: string;
  name_en: string;
  short_description_ru: string;
  short_description_en: string;
  description_ru: string;
  description_en: string;
  active: boolean;
  created_ts: string;
}

export interface ApiSportComplexResponse {
  total_count: number;
  list: ApiSportComplex[];
}
