export function getUrlPrefixedApi<T extends object & { [key: string]: any }>(
  instance: T,
  prefix: string,
  methods: any[] = ["get", "post", "put", "delete"]
) {
  return new Proxy<T>(instance, {
    get(target, property: string, receiver) {
      if (!methods.includes(property)) {
        return Reflect.get(target, property, receiver);
      }

      return new Proxy(target[property], {
        apply(target, thisArg, argList) {
          const newArgList = argList.slice();
          if (typeof newArgList[0] === "string") {
            newArgList[0] = `${prefix}${newArgList[0]}`;
          }
          return Reflect.apply(target, thisArg, newArgList);
        },
      });
    },
  });
}
